Script for automatic installation and configuration Munin Monitoring Tool on Ubuntu
===

Usage
---
This script install a few packages and therefore you should run it under sudo privilegous.
There are some parameters that you can pass to script

    -m  -   user mail 
    
    -h  -   host name, that will show on munin web page
    
    -d  -   path to user defined configs. If you configuration files placed in diffrent from current directory
    you need to specify them.

Examples
---
    git clone https://gitlab.com/haqler/munin
    
    cd munin
    
    sudo ./munin.sh -h MyHostName -m MyMail
    
If mail not supplied by flaq -m then script will ask user to input it on installation.

How it works
---
Step by step

1. **Check user input and set global variables**

2. **Install packages.**

    Here script check whether needed packages exist in system or not
    and based on it create list of packages that should be installed.

3. **Configuration of munin**

    At this step user config files will be copied to munin main config directory `/etc/munin`.
    
    First, script will check
    
        does files apache24.conf and munin.conf exist in directory /etc/munin
        
        does user speciefid apache24.conf, munin.conf files exist.

    If all is okay, then script will create backups of existing munin configs(in the same directory) and copy user configs to /etc/munin.
    
    At this moment mail and host name supplied by user will be added to munin.conf file, other parameters specified in user munin.conf file won't be overwritten.
    
    Therefore you can add any configs to your default munin.conf file.
    
    To add mail and host to munin.conf here is used sed command, which change patterns `user_mail` and `localhost.localdomain`
    
    in user's munin.conf file to new entered values.
    
        sed 's/user_mail/user_entered_value_for_mail/'
        set 's/localhost.localdomain/user_entered_value_for_host/'
    
4. **Runing**

    Here apache and munin will be restarted.

After script finish you can see result in brower, just type `your_ip/munin` (for example `localhost/munin`).


Usefull links
---
1. ![How To Install the Munin Monitoring Tool on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-munin-monitoring-tool-on-ubuntu-14-04)
2. ![Set thresholds values](https://www.server-world.info/en/note?os=CentOS_7&p=munin&f=3)
3. ![Mail installation](https://www.digitalocean.com/community/tutorials/how-to-install-and-setup-postfix-on-ubuntu-14-04)
4. ![Send notifications](https://www.server-world.info/en/note?os=CentOS_7&p=munin&f=2)
5. ![Munin Documantation pdf](https://media.readthedocs.org/pdf/munin/latest/munin.pdf)
